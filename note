#!/usr/bin/perl
use strict;
use warnings;
use Device::SerialPort;
use Time::HiRes qw(usleep nanosleep);
use Digest::CRC qw(crcccitt);
use Carp;

my $port = Device::SerialPort->new("/dev/ttyUSB0") or die "Unable to open port";

$port->baudrate(9600);
$port->databits(8);
$port->parity("even");
$port->stopbits(1);
$port->handshake("none");

$port->read_const_time(10);

#width=16 poly=0x1021 init=0x0000 refin=true refout=true xorout=0x0000 check=0x2189 residue=0x0000 name="KERMIT"

sub calcCrc {
    my ($msg) = @_;

    my $kermit = Digest::CRC->new(width=>16, init=>0x0000, xorout=>0x0000, 
				  refout=>1, poly=>0x1021, refin=>1, cont=>1);

    $kermit->add($msg);
    my $crc = $kermit->digest;

    my $crcLow  = $crc & 0xff;
    my $crcHigh = $crc >> 8;

    return sprintf("%c%c", $crcLow, $crcHigh);
}

sub cmd {
    my $str = sprintf("%c", 0xfc);

    $str .= sprintf("%c", scalar(@_)+4);

    for my $byte (@_) {
	$str .= sprintf("%c", $byte);	
    }

    return $str . calcCrc($str);
}

sub toHex {
    my $str = shift @_;
    confess "Cannot convert undef to hex" unless defined $str;
    my @bytes = map {ord} split //, $str;
    return join ' ', map {sprintf " %02x", $_;} @bytes;
}

my %RESPONSE_NAMES = (
    0x11=>'Idle',
    0x12=>'Accepting',
    0x13=>'Escrow',
    0x14=>'Stacking',
    0x15=>'Vend Valid',
    0x16=>'Stacked',
    0x17=>'Rejecting',
    0x18=>'Returning',
    0x19=>'Holding',
    0x1a=>'Inhibit',
    0x1b=>'Init',

    0x40=>'Power up',
    0x41=>'Power up with bill in acceptor',
    0x42=>'Power up with bill in stacker',

    0x43=>'Error: Stacker full',
    0x44=>'Error: Stacker open (stacker box removed)',
    0x45=>'Error: Jam in acceptor',
    0x46=>'Error: Jam in stacker',
    0x47=>'Error: Pause',
    0x48=>'Error: Cheated',
    0x49=>'Error: Failure',

    0x4A=>'Error: Communication error',
    0x4b=>'Error: Invalid command',

    0x88=>'Version info',
);

my %FAILURES = (
    0xa2 => 'Stack motor',
    0xa5 => 'Transport feed motor speed failure',
    0xa6 => 'Transport feed motor failure',
    0xa8 => 'Solenoid',
    0xa9 => 'PB Unit',
    0xab => 'Cash box not ready',
    0xaf => 'Validator head removed',
    0xb0 => 'BOOT ROM',
    0xb1 => 'External ROM',
    0xb2 => 'RAM',
    0xb3 => 'External ROM write',
);

my %REJECT = (
    0x71 => 'Insertion error',
    0x72 => 'Mug error',
    0x73 => 'Return action due to residual bills, etc. (at the head part of ACCEPTOR)',
    0x74 => 'Calibration error/ Magnification error',
    0x75 => 'Conveying error',
    0x76 => 'Discrimination error for bill denomination',
    0x77 => 'Photo pattern error 1',
    0x78 => 'Photo level error',
    0x79 => 'Return by INHIBIT: Error of insertion direction / Error of bill denomination',
    0x7A => 'No command sent answering to ESCROW',
    0x7b => 'Operation error',
    0x7c => 'Return action due to residual bills, etc. (at the stacker)',
    0x7D => 'Length error',
    0x7E => 'Photo pattern error 2',
    0x7F => 'True bill feature error',
    );

my %NOTE = (
    0x63=>50,
    0x64=>100,
    0x65=>200,
    0x66=>500,
);

sub parse {
    my $str = shift @_;

    my ($fc, $len, $cmd, $payload, $crc) =
	$str =~ /^(.)(.)(.)(.*)(..)$/ or return {};

    my $okcrc = calcCrc("$fc$len$cmd$payload");
    my $ocmd = ord($cmd);
    my $name = $RESPONSE_NAMES{$ocmd};
    if ($ocmd == 0x49 and length($payload) == 1) {
	$name .= " ".($FAILURES{ord($payload)} || '?');
    }

    if ($ocmd == 0x17 and length($payload) == 1) {
	$name .= " ".($REJECT{ord($payload)} || '?');
    }
    
    if ($ocmd == 0x88) {
	$name .= $payload;
    }

    if ($ocmd == 0x13) {
	$name .= " ".($NOTE{ord($payload)} || toHex($payload));
    }
    
    return {	
	okcrc => toHex($okcrc),
	crc=> toHex($crc),
	fc=>ord($fc),
	len=>ord($len),
	cmd=>$ocmd,
	payload=>$payload,
	name=>$name,
    };
}

my $ownCmd = cmd(0x11);
#die toHex($statusCmd)."\n".toHex($ownCmd)."\n";

my @cmds = (
#    cmd(0x88), # Get version
    cmd(0x40), # Reset
  #  cmd(0x07), # wtf?
    cmd(0xc0, 0, 0), # Enable
    cmd(0x8a), # Get curreny assignments
    cmd(0xc3, 0), # Inhibit
);

my $ec = 40;
my $cc = 0;
my $lastStatus = "";
while (1) {
    my $cmd;
    
    if ($ec++ > 50) {
	$ec = 0;
	if ($cc < @cmds) {
	    $cmd = $cmds[$cc++];
	    print "Sending ".toHex($cmd);
	} else {
	    $cmd = cmd(0x11);
	}
	$lastStatus = "";
	
    } else {
	$cmd = cmd(0x11);
    }

    $port->write($cmd) or die "Failed to send ".toHex($cmd);
	
    
    my ($bytes, $str) = $port->read(100);

    
    if ($bytes > 0 and length($str)) {
	my $parsed = parse($str);
	if ($parsed->{okcrc} eq $parsed->{crc}) {
	    if ($parsed->{name}) {
		if ($lastStatus ne $parsed->{name}) {
		    print " Got $ec : $bytes bytes: ".toHex($str)." $parsed->{name}\n";
		    $lastStatus = $parsed->{name};
		}

		if ($parsed->{cmd} == 0x13) {
		    push @cmds, cmd(0x41); # Stack
		    $ec = 1000;

		} elsif ($parsed->{cmd} == 0x15) {
		    push @cmds, cmd(0x50); # ACK
		    $ec = 1000;
		}
	    } else {
		print " Got $ec : $bytes bytes: ".toHex($str)."\n";
	    }
	    
	} else {
	    print "Bad crc: $parsed->{okcrc} ne $parsed->{crc}\n";
	}
    } else {
	print " No answer\n";
    }
    
    usleep 100*1000;
}
