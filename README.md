# cashbox


Contents:

* pc104 geode based industial PC of some sort, used to run windows XP, will be running Linux.
* Note acceptor: japan cashmachine EBA-21-pb-eur2-105-003 (serial)
* Coin Acceptor: Mei Cashflow 9520e (can be configured for ccTalk serial) 
* Printer: TG58-T (serial)
* Touch screen on lvds, also available via VGA.

# Note acceptor

The note acceptor model is officially the pulse mode, but it's wired for serial.



# Printer

34800 8N1
xon/xoff

1: GND
2: TX
3: RX


# Coin acceptor

## Power+Output

1: GND
2: +12V
3: Signal GND
4: Output Coin A
5: Output Coin B
6: Output Coin C
7: Output Coin D
8: Output Coin E
9: Output Coin F

## Inhibit

1: Inhibit Coint A
2: Inhibit Coint B 
3: Inhibit Coint C
4: Inhibit Coint D
5: Inhibit Coint E
6: Inhibit Coint F


# Note acceptor

1: GND   2
2: GND   4
3: +12V  1
4: +12V  3
5: n/c
6: n/c
7: TX    25
8: RX    26
